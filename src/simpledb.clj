(ns simpledb
  (:refer-clojure :exclude [find count mapcat])
  (:require [clojure.data :as data]
            [clojure.set :as set]
            [taoensso.timbre :as log]
            [cognitect.aws.client.api :as aws]
            [cognitect.aws.signers.v2]
            [honeysql.core :as sql]
            [simpledb.util :as util]))

;; -------
;;  criteria: select, query, find, exists, remove!
;;
;; -----

(defn process-response
  "look for problems as reported by cognitect response map"
  [{:keys [:cognitect.anomalies/category :cognitect.anomalies/message :cognitect.anomalies/throwable] :as response}]
  (when category
    (log/warnf "AWS invocation anomaly category [%s]" category)
    (when-not (empty? message) (log/warn message))
    (if throwable
      (throw throwable)
      (log/warn response)))
  response)

(defn prepare-select-expression
  "merge the sql-v statement with its subsequent args if any"
  [[sql & args :as sql-v]]
  (reduce
   (fn [s arg]
     (let [sanitized-arg (str "'"  (str arg) "'")]
       (clojure.string/replace-first s #"[?]" sanitized-arg)))
   sql
   args))

;; -----
;; lazier mapcat
;;  -- using this version ensures that we only hit the api endpoint as needed
;;
;; see below for how the core/mapcat works (forces multiple evaluations of lazy fns)
;; taken from : https://clojuredocs.org/clojure.core/mapcat (note 1) -- mdp
;;
;; -----
(defn mapcat [f & colls]
  (letfn [(step [colls]
            (lazy-seq
             (when-first [c colls]
               (concat c (step (rest colls))))))]
    (step (apply map f colls))))

(defn select*
  "lazily calls the aws sdb 'select' api endpoint concatenating the items returned"
  [client request]
  (letfn [(select-fn [req]
            (process-response (aws/invoke client req)))
          (next-fn [{next-token :NextToken :as resp}]
            (when next-token
              (select-fn (assoc-in request [:request :NextToken] next-token))))]
    (->> (select-fn request)
         (iterate next-fn)
         (take-while (complement empty?))
         (mapcat :Items))))

(defn query
  "execute a honeysql map OR a clojure.java.jdbc sql vector (select only!)"
  ([client sql-map params]
   (query client (sql/format sql-map params :allow-dashed-names? true :quoting :mysql)))
  ([client sql-v]
   (let [select-expression (prepare-select-expression sql-v)]
     (select* client {:op :Select :request {:SelectExpression select-expression
                                            :ConsistentRead true}}))))

(defn add-value
  "handles adding values to multi-valued keys as vectors"
  [current value]
  (if  current
    (if (vector? current)
      (conj current value)
      [current value])
    value))

(defn prepare-item
  "builds a map of an sdb item's attribute names and values with meta data of the original information"
  [{name :Name attributes :Attributes :as item}]
  (let [item (reduce
              (fn [m {name :Name value :Value}]
                (let [kw (keyword name)
                      current (get m kw)
                      v (add-value current value)]
                  (assoc m kw v)))
              {}
              attributes)]
    (with-meta item {:name name :original item})))

(defn select
  "select from domain based on a sequence of criteria"
  [client domain & criteria]
  (let [sql-v (sql/format (apply util/select-map domain criteria) :allow-dashed-names? true :quoting :mysql)]
    (log/debug sql-v)
    (->> (query client sql-v)
         (map prepare-item))))

(defn find [client domain & criteria]
  (first (apply select client domain criteria)))

(defn find-by-id [client domain id]
  (find client domain {:id id}))

(defn count [client domain & criteria]
  (let [sql-v (sql/format (apply util/exists-map domain criteria) :allow-dashed-names? true :quoting :mysql)]
    (log/debug sql-v)
    (clojure.edn/read-string (->> (query client sql-v)
                                  first
                                  :Attributes
                                  first
                                  :Value))))

(defn exists? [client domain & criteria]
  (let [count (apply count client domain criteria)]
    (> count 0)))

(defn prepare-expectation [{name :Name value :Value :as expectation}]
  (cond-> (assoc expectation :Name (clojure.core/name name))
    value  (assoc :Value (str value))))

;; -------
;;  deletion
;;
;; -----

(defn prepare-attribute-for-deletion-dispatch-fn [[_ v]]
  (class v))

(defmulti prepare-attribute-for-deletion #'prepare-attribute-for-deletion-dispatch-fn)

(defmethod prepare-attribute-for-deletion clojure.lang.Sequential [[k v]]
  (reduce
   (fn [attrs element]
     (conj attrs {:Name (name k)
                  :Value (str element)}))
   []
   v))

(defmethod prepare-attribute-for-deletion :default [[k v]]
  [{:Name (name k)
    :Value (str v)}])

(defn prepare-attributes-for-deletion [rec]
  (reduce
   (fn [attributes entry]
     (into attributes (prepare-attribute-for-deletion entry) ))
   []
   rec))

(defn delete-attributes!
  ([client domain identifier rec]
   (process-response (aws/invoke client {:op :DeleteAttributes :request {:DomainName (name domain)
                                                                         :Attributes (prepare-attributes-for-deletion rec)
                                                                         :ItemName identifier}})))
  ([client domain identifier rec expectation]
   (process-response (aws/invoke client {:op :DeleteAttributes :request {:DomainName (name domain)
                                                                         :Attributes (prepare-attributes-for-deletion rec)
                                                                         :ItemName identifier
                                                                         :Expected (prepare-expectation expectation)}}))))

(defn remove-attributes!
  ([client domain {:keys [id] :as rec} attributes] {:pre [(some? id)]}
   (delete-attributes! client domain id (select-keys rec attributes)))
  ([client domain {:keys [id] :as rec} attributes expectation] {:pre [(some? id)]}
   (delete-attributes! client domain id (select-keys rec attributes) expectation)))

(defn delete-record!
  ([client domain identifier]
   (process-response (aws/invoke client {:op :DeleteAttributes :request {:DomainName (name domain)
                                                                         :ItemName identifier}})))
  ([client domain identifier expectation]
   (process-response (aws/invoke client {:op :DeleteAttributes :request {:DomainName (name domain)
                                                                         :ItemName identifier
                                                                         :Expected (prepare-expectation expectation)}}))))

(defn delete!
  ([client domain {:keys [id] :as rec}] {:pre [(some? id)]}
   (delete-record! client domain id))
  ([client domain {:keys [id] :as rec} expectation] {:pre [(some? id)]}
   (delete-record! client domain id expectation)))

(defn prepare-items-and-attributes-for-deletion [recs]
  (mapv
   (fn [{:keys [id] :as rec}]
     (merge
      {:Name (str id)}
      (when-not (empty? (dissoc rec :id))
        {:Attributes (prepare-attributes-for-deletion rec)})))
   recs))

(defn batch-delete-attributes! [client domain recs]
  (process-response (aws/invoke client {:op :BatchDeleteAttributes
                                        :request {:DomainName (name domain)
                                                  :Items (prepare-items-and-attributes-for-deletion recs)}})))


(def BATCH_DELETE_ITEM_LIMIT 25) ;; see api docs
(defn remove!
  "find all record ids that match the criteria given, batch delete the records with those ids, return the removed ids"
  [client domain & criteria]
  (let [sql-v (sql/format (apply util/delete-map domain criteria) :allow-dashed-names? true :quoting :mysql)
        _ (log/debug sql-v)
        recs (->> (query client sql-v)
                  (map prepare-item))]
    (run!
     (partial batch-delete-attributes! client domain)
     (partition-all BATCH_DELETE_ITEM_LIMIT recs))
    (mapv :id recs)))

;; -------
;;  update/insert
;;
;; -----

(defn prepare-attribute-for-storage-dispatch-fn [[_ v]]
  (class v))

(defmulti prepare-attribute-for-storage #'prepare-attribute-for-storage-dispatch-fn)

(defmethod prepare-attribute-for-storage clojure.lang.Sequential [[k v]]
  (reduce
   (fn [attrs element]
     (conj attrs {:Name (name k)
                  :Value (str element)
                  :Replace false}))
   []
   v))

(defmethod prepare-attribute-for-storage :default [[k v]]
  [{:Name (name k)
    :Value (str v)
    :Replace true}])

(defn prepare-attributes-for-storage [rec]
  (reduce
   (fn [attributes entry]
     (into attributes (prepare-attribute-for-storage entry) ))
   []
   rec))

(defn put-attributes!
  ([client domain identifier rec]
   (process-response (aws/invoke client {:op :PutAttributes :request {:DomainName (name domain)
                                                                      :ItemName identifier
                                                                      :Attributes (prepare-attributes-for-storage rec)}})))
  ([client domain identifier rec expectation]
   (process-response (aws/invoke client {:op :PutAttributes :request {:DomainName (name domain)
                                                                      :ItemName identifier
                                                                      :Attributes (prepare-attributes-for-storage rec)
                                                                      :Expected (prepare-expectation expectation)}}))))

(defn insert!
  ([client domain identifier {:keys [id] :as rec}] {:pre [(nil? id)]}
   (put-attributes! client domain identifier (assoc rec :id identifier)))
  ([client domain identifier {:keys [id] :as rec} expectation] {:pre [(nil? id)]}
   (put-attributes! client domain identifier  (assoc rec :id identifier) expectation)))



;; -----
;;   Note: a deletion keys is ONLY needed in two cases:
;;
;;         1. the key refer to a multi-valued attribute (always)
;;              -- in the case where we also have an addition for this mva, we remove
;;                 the complete set of values for that attribute and replace them with
;;                 a new set (in additons)
;;              -- in the case where there is no addition for this attribute it will be a removal
;;
;;         2. the key is not multi-valued but is also not found in the additions; it will be a removal
;;
;;          otherwise the addition will overwrite the old value and we can ignore the deletion
;; --mdp
;;

(defn changes [rec]
  (letfn [(keys-to-delete [m akeys-set dkeys]
            (keep
             (fn [[k v]]
               (when (or (vector? v) (set? v) (seq? v) (not (akeys-set k))) k))
             (select-keys m dkeys)))]
    (let [original (get (meta rec) :original)
          [a d] (data/diff rec original)
          aks (keys a)
          dks (keys-to-delete original (set aks) (keys d))
          additions (select-keys rec aks)
          deletions (select-keys original dks)]
      [additions deletions])))

;; -----
;; NOTE: for update! .. the expectation (when used) can only be used on the second call (the additions) if and only if
;; it is not the case that the attribute being checked is one of the deletions otherwise the expectation will fail since it has been deleted
;; (ask me how I know ;-) ) --mdp
;;

(defn update!
  ([client domain {:keys [id] :as rec}] {:pre [(some? id)]}
   (let [[additions deletions] (changes rec)]
     (when-not (empty? deletions) (delete-attributes! client domain id deletions))
     (when-not (empty? additions) (put-attributes! client domain id additions))))
  ([client domain {:keys [id] :as rec} {:keys [Name] :as expectation}] {:pre [(some? id)]}
   (let [[additions deletions] (changes rec)]
     (when-not (empty? deletions) (delete-attributes! client domain id deletions expectation))
     (when-not (empty? additions)
       (let [expectation-deleted? (some
                                   (fn [k]
                                     (= (name Name) (name k)))
                                   (keys deletions))]
         (if-not expectation-deleted?
           (put-attributes! client domain id additions expectation)
           (put-attributes! client domain id additions)))))))
