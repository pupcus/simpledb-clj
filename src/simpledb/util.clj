(ns simpledb.util
  (:require [honeysql.helpers :as helpers]
            [honeysql.format :refer [format-clause]]
            [clojure.string :as str]))

(defn ensure-vec [x]
  (if (vector? x)
    x
    [x]))

(defmethod format-clause :from [[_ tables] _]
  (str "FROM " (str/join "," (map (fn [table]
                                    (str "`" (name table) "`"))
                                  tables))))

(defn uuid [] (str (java.util.UUID/randomUUID)))

(defn now []
  (java.time.Instant/now))

(defn- criteria-dispatch [_ c]
  (class c))

(defmulti criteria #'criteria-dispatch)

(defmethod criteria clojure.lang.APersistentMap [m c]
  (reduce criteria m c))

(defmethod criteria clojure.lang.APersistentVector [m c]
  (if (= (count c) 2)
    (helpers/merge-where m (into [:=] c))
    (helpers/merge-where m c)))

(defmethod criteria :default [_ c]
  (throw (IllegalStateException. "Ill formed criteria:  [%s]" c)))

(defn query-map
  "build a honeysql map to select * from the simpleDb domain reducing criteria to a where clause"
  [domain selection & c]
  (reduce
   criteria
   (-> (helpers/select selection)
       (helpers/from domain))
   c))

(defn select-map
  "build a honeysql map to select * from domain reducing criteria to a where clause"
  [domain & c]
  (apply query-map domain :* c))

(defn exists-map
  "build a honeysql map to select * from domain reducing criteria to a where clause"
  [domain & c]
  (apply query-map domain :%count.* c))

;;
;; NOTE: in simbpledb record deletions must be done via delete-attribute calls and NOT via a sql command.
;;       simpledb sql is ONLY a query interface (so far) and is not likely to change.
;;
;;  We can emulate the 'delete from domain where <criteria>' sql by
;;  selecting all the ids that match the criteria and deleting those in a second step. --mdp
;;  (see remove! in simpledb.clj)
;;
(defn delete-map
  "build a honeysql map to select from domain reducing criteria to a where clause"
  [domain & c]
  (apply query-map domain :id c))
