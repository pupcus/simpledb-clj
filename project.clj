(defproject simpledb-clj "0.0.19-SNAPSHOT"

  :description "utility layer above aws simpledb"

  :url "https://bitbucket.org/pupcus/simpledb-clj"

  :scm {:url "git@bitbucket.org:pupcus/simpledb-clj"}

  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[honeysql                     "1.0.461"]
                 [com.taoensso/timbre          "5.1.2"]
                 [com.cognitect.aws/signers-v2 "0.0.2"]]

  :deploy-repositories [["snapshots"
                         {:url "https://clojars.org/repo"
                          :sign-releases false
                          :creds :gpg}]
                        ["releases"
                         {:url "https://clojars.org/repo"
                          :sign-releases false
                          :creds :gpg}]]

  :release-tasks [["vcs" "assert-committed"]
                  ["change" "version" "leiningen.release/bump-version" "release"]
                  ["vcs" "commit"]
                  ["vcs" "tag" "--no-sign"]
                  ["deploy"]
                  ["change" "version" "leiningen.release/bump-version"]
                  ["vcs" "commit"]
                  ["vcs" "push"]]

  :profiles {:dev {:dependencies [[org.clojure/clojure     "1.10.3"]
                                  [com.cognitect.aws/api        "0.8.524"]
                                  [com.cognitect.aws/endpoints  "1.1.12.110"]
                                  [com.cognitect.aws/sdb        "770.2.568.0"]]}})
