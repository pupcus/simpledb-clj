(ns delete-attribute-test
  (:require [clojure.test :refer [deftest is testing]]
            [cognitect.aws.client.api :as aws]
            [helpers :as h]
            [simpledb :as sdb]))

(def client (aws/client (merge {:api :sdb} h/client-overrides)))

(deftest test-delete-attributes!!

  (let [response (h/build-sdb-response {:op :DeleteAttributes})
        id "UNIQUE-ID"
        rec {:id id :a1 "aaaaaa" :b1 "bbbbbb"}
        expectation {:Name :id :Value id}]

    (testing "deleting specific attributes from a record"

      (h/with-captured-requests requests {:DeleteAttributes response}

        (sdb/delete-attributes! client :domain id rec)

        (is (= {"AWSAccessKeyId" "MOCKACCESSKEYEXAMPLE"
                "Action" "DeleteAttributes"
                "Attribute.1.Name" "id"
                "Attribute.1.Value" "UNIQUE-ID"
                "Attribute.2.Name" "a1"
                "Attribute.2.Value" "aaaaaa"
                "Attribute.3.Name" "b1"
                "Attribute.3.Value" "bbbbbb"
                "DomainName" "domain"
                "ItemName" "UNIQUE-ID"
                "Signature" "co4kgUOl%2FU2XkX6XCAZjrzFTZ8qe5roBwHe3CLf3%2Fgw%3D"
                "SignatureMethod" "HmacSHA256"
                "SignatureVersion" "2"
                "Timestamp" "2020-06-16T09%3A12%3A40.316Z"
                "Version" "2009-04-15"}

               (-> (get @requests :DeleteAttributes)
                   first
                   (get-in [:op-map :body-as-str])
                   h/body-params-as-map)))))

    (testing "deleting specific attributes from a record with an expectation"

      (h/with-captured-requests requests {:DeleteAttributes response}

        (sdb/delete-attributes! client :domain id rec expectation)

        (is (= {"AWSAccessKeyId" "MOCKACCESSKEYEXAMPLE"
                "Action" "DeleteAttributes"
                "Attribute.1.Name" "id"
                "Attribute.1.Value" "UNIQUE-ID"
                "Attribute.2.Name" "a1"
                "Attribute.2.Value" "aaaaaa"
                "Attribute.3.Name" "b1"
                "Attribute.3.Value" "bbbbbb"
                "DomainName" "domain"
                "Expected.Name" "id"
                "Expected.Value" "UNIQUE-ID"
                "ItemName" "UNIQUE-ID"
                "Signature" "TnQTFbu9Hn3yIlUeDMbB4%2FKMIPELmy90zEHD7ZUF8zY%3D"
                "SignatureMethod" "HmacSHA256"
                "SignatureVersion" "2"
                "Timestamp" "2020-06-16T09%3A12%3A40.316Z"
                "Version" "2009-04-15"}

               (-> (get @requests :DeleteAttributes)
                   first
                   (get-in [:op-map :body-as-str])
                   h/body-params-as-map)))))))
