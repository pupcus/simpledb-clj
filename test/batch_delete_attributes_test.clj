(ns batch-delete-attributes-test
  (:require [clojure.test :refer [deftest is testing]]
            [cognitect.aws.client.api :as aws]
            [helpers :as h]
            [simpledb :as sdb]))

(def client (aws/client (merge {:api :sdb} h/client-overrides)))

(deftest test-batch-delete-attributes!!

  (let [response (h/build-sdb-response {:op :BatchDeleteAttributes})]

    (testing "deleting specific records by id"
      (let [recs (mapv (fn [i] {:id i}) (range 1 4))]
        (h/with-captured-requests requests {:BatchDeleteAttributes response}

          (sdb/batch-delete-attributes! client :domain recs)

          (is (= {"AWSAccessKeyId" "MOCKACCESSKEYEXAMPLE"
                  "Action" "BatchDeleteAttributes"
                  "DomainName" "domain"
                  "Item.1.ItemName" "1"
                  "Item.2.ItemName" "2"
                  "Item.3.ItemName" "3"
                  "Signature" "%2FyP7KQsravTzlaDi7tkoHJpqGTqaHYfoPS6U1CpHYy8%3D"
                  "SignatureMethod" "HmacSHA256"
                  "SignatureVersion" "2"
                  "Timestamp" "2020-06-16T09%3A12%3A40.316Z"
                  "Version" "2009-04-15"}
                 (-> (get @requests :BatchDeleteAttributes)
                     first
                     (get-in [:op-map :body-as-str])
                     h/body-params-as-map))))))))
