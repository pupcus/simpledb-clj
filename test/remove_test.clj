(ns remove-test
  (:require [clojure.test :refer [deftest is testing]]
            [cognitect.aws.client.api :as aws]
            [helpers :as h]
            [simpledb :as sdb]))

(def client (aws/client (merge {:api :sdb} h/client-overrides)))

(deftest test-remove!

  (testing "when deleting records by criteria the delete batches are sized to 25 per batch"

    (let [select-response (h/build-sdb-response {:op :Select
                                                 :response
                                                 [:SelectResult
                                                  (vec (mapcat
                                                        (fn [i]
                                                          [:Item
                                                           [:Name (str i)
                                                            :Attribute [:Name "id" :Value (str i)]]])
                                                        (range 1 27)))]})
          batch-delete-response (h/build-sdb-response {:op :BatchDeleteAttributes})]

      (h/with-captured-requests requests {:Select select-response
                                          :BatchDeleteAttributes batch-delete-response}

        (let [response (sdb/remove! client :domain {:attr "value"})]
          (is (= (mapv str (range 1 27))
                 response)) )

        (is (= 1
               (count (get @requests :Select))))

        (is (= 2
               (count (get @requests :BatchDeleteAttributes))))

        (is (= {"AWSAccessKeyId" "MOCKACCESSKEYEXAMPLE"
                "Action" "BatchDeleteAttributes"
                "DomainName" "domain"
                "Item.1.ItemName" "1"
                "Item.10.ItemName" "10"
                "Item.11.ItemName" "11"
                "Item.12.ItemName" "12"
                "Item.13.ItemName" "13"
                "Item.14.ItemName" "14"
                "Item.15.ItemName" "15"
                "Item.16.ItemName" "16"
                "Item.17.ItemName" "17"
                "Item.18.ItemName" "18"
                "Item.19.ItemName" "19"
                "Item.2.ItemName" "2"
                "Item.20.ItemName" "20"
                "Item.21.ItemName" "21"
                "Item.22.ItemName" "22"
                "Item.23.ItemName" "23"
                "Item.24.ItemName" "24"
                "Item.25.ItemName" "25"
                "Item.3.ItemName" "3"
                "Item.4.ItemName" "4"
                "Item.5.ItemName" "5"
                "Item.6.ItemName" "6"
                "Item.7.ItemName" "7"
                "Item.8.ItemName" "8"
                "Item.9.ItemName" "9"
                "Signature" "2ZeDYi8ujbPnL5ELqgmaSiq%2Baw%2BzmVzR77bAlUadTnc%3D"
                "SignatureMethod" "HmacSHA256"
                "SignatureVersion" "2"
                "Timestamp" "2020-06-16T09%3A12%3A40.316Z"
                "Version" "2009-04-15"}

               (-> (get @requests :BatchDeleteAttributes)
                   first
                   (get-in [:op-map :body-as-str])
                   h/body-params-as-map)))

        (is (= {"AWSAccessKeyId" "MOCKACCESSKEYEXAMPLE"
                "Action" "BatchDeleteAttributes"
                "DomainName" "domain"
                "Item.1.ItemName" "26"
                "Signature" "rFbExOePPo5%2FEyy%2BxSepAkAXrgWrGhZR%2BMhmO8m2qF4%3D"
                "SignatureMethod" "HmacSHA256"
                "SignatureVersion" "2"
                "Timestamp" "2020-06-16T09%3A12%3A40.316Z"
                "Version" "2009-04-15"}

               (-> (get @requests :BatchDeleteAttributes)
                   second
                   (get-in [:op-map :body-as-str])
                   h/body-params-as-map)))))))
