(ns select-test
  (:require [clojure.test :refer [deftest is testing]]
            [cognitect.aws.client.api :as aws]
            [helpers :as h]
            [simpledb :as sdb]))

(def client (aws/client (merge {:api :sdb} h/client-overrides)))

(deftest test-select-api-call

  (testing "sending a select sql query"

    (h/with-captured-requests requests {:Select (h/build-sdb-response {:op :Select
                                                                       :response
                                                                       [:SelectResult
                                                                        [:Item
                                                                         [:Name "name1"
                                                                          :Attribute
                                                                          [:Name "n1" :Value "v1"]
                                                                          :Attribute
                                                                          [:Name "n2" :Value "v2"]]
                                                                         :Item
                                                                         [:Name "name2"
                                                                          :Attribute
                                                                          [:Name "n3" :Value "v3"]
                                                                          :Attribute
                                                                          [:Name "n4" :Value "v4"]]]]})}

      (let [results (sdb/select* client {:op :Select :request {:SelectExpression "select * from test"
                                                               :ConsistentRead true}})]

        (is (= {"AWSAccessKeyId" "MOCKACCESSKEYEXAMPLE"
                "Action" "Select"
                "ConsistentRead" "true"
                "SelectExpression" "select%20%2A%20from%20test"
                "Signature" "vfrFUG0iH5lHGCLEm3n8ZqOfeinPzU2DxrDvm%2BiE4Lw%3D"
                "SignatureMethod" "HmacSHA256"
                "SignatureVersion" "2"
                "Timestamp" "2020-06-16T09%3A12%3A40.316Z"
                "Version" "2009-04-15"}

               (-> (get @requests :Select)
                   first
                   (get-in [:op-map :body-as-str])
                   h/body-params-as-map)))

        (is (= [{:Name "name1"
                 :Attributes
                 [{:Name "n1" :Value "v1"}
                  {:Name "n2" :Value "v2"}]}
                {:Name "name2"
                 :Attributes
                 [{:Name "n3" :Value "v3"}
                  {:Name "n4" :Value "v4"}]}]
               results))))))
