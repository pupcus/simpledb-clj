(ns simpledb-test
  (:require [clojure.test :refer [deftest is testing]]
            [simpledb :as sdb]))

(deftest test-prepare-select-expression

  (testing "preparing a sql select expression for simpledb with no args"
    (is (= "select * from table where column1 = '1' AND column2 = '2'"
           (sdb/prepare-select-expression ["select * from table where column1 = '1' AND column2 = '2'"]))))

  (testing "preparing a sql select expression for simpledb ensures all args are quoted"
    (is (= "select * from table where column1 = '1' AND column2 = '2'"
           (sdb/prepare-select-expression ["select * from table where column1 = ? AND column2 = ?" 1 2])))))


(deftest add-value-test

  (testing "combining multiple valued 'values' for a key"

    (is (= [:current :value]
           (sdb/add-value :current :value)))

    (is (= [:a :b :c]
           (sdb/add-value [:a :b] :c)))

    (is (= :value
           (sdb/add-value nil :value)))))


(deftest prepare-item-test

  (testing "building record maps from results of api select queries"

    (let [item {:Name "id1" :Attributes [{:Name "id" :Value "id1"}]}
          rec (sdb/prepare-item item)]
      (is (= {:id "id1"}
             rec))

      (is (= {:name "id1" :original rec}
             (meta rec)))))

  (testing "building record maps from results of api select queries with multiple valued attributes"

    (let [item {:Name "id1" :Attributes [{:Name "id" :Value "id1"}
                                         {:Name "a" :Value "1"}
                                         {:Name "a" :Value "2"}
                                         {:Name "a" :Value "3"}
                                         {:Name "a" :Value "4"}]}
          rec (sdb/prepare-item item)]
      (is (= {:id "id1" :a ["1" "2" "3" "4"]}
             rec))

      (is (= {:name "id1" :original rec}
             (meta rec))))))


(deftest changes-test

  (testing "generating additions and deletions for a rec that has chalnged from its original (stored in meta)"

    (testing "simple deletion"
      (let [orig {:id 1 :a "a" :b "b"}
            rec (with-meta {:id 1 :a "a"} {:name 1 :original orig})
            [additions deletions] (sdb/changes rec)]

        (is (= {}
               additions))

        (is (= {:b "b"}
               deletions))))


    (testing "simple addition"
      (let [orig {:id 1 :a "a"}
            rec (with-meta {:id 1 :a "a" :b "b"} {:name 1 :original orig})
            [additions deletions] (sdb/changes rec)]

        (is (= {:b "b"}
               additions))

        (is (= {}
               deletions))))

    (testing "multi-valued changes"
      (let [orig {:id 1 :a [1 2 3 4]}
            rec (with-meta {:id 1 :a [4 6 5 2]} {:name 1 :original orig})
            [additions deletions] (sdb/changes rec)]

        (is (= {:a [4 6 5 2]}
               additions))

        (is (= {:a [1 2 3 4]}
               deletions))))


    (testing "multi-valued changes with strings"
      (let [orig {:id 1 :a ["1" "2" "3" "4"]}
            rec (with-meta {:id 1 :a ["4" "6" "5" "2"]} {:name 1 :original orig})
            [additions deletions] (sdb/changes rec)]

        (is (= {:a ["4" "6" "5" "2"]}
               additions))

        (is (= {:a ["1" "2" "3" "4"]}
               deletions))))


    (testing "multi-valued changes all removed"
      (let [orig {:id 1 :a [1 2 3 4]}
            rec (with-meta {:id 1} {:name 1 :original orig})
            [additions deletions] (sdb/changes rec)]

        (is (= {}
               additions))

        (is (= {:a [1 2 3 4]}
               deletions))))


    (testing "multi-valued changes all added"
      (let [orig {:id 1}
            rec (with-meta {:id 1 :a [1 2 3 4]} {:name 1 :original orig})
            [additions deletions] (sdb/changes rec)]

        (is (= {:a [1 2 3 4]}
               additions))

        (is (= {}
               deletions))))


    (testing "string changes"
      (let [orig {:id 1 :a "this is a test" :b "so is this"}
            rec (with-meta {:id 1 :a "this is a test of the emergency broadcast system"} {:name 1 :original orig})
            [additions deletions] (sdb/changes rec)]

        (is (= {:a "this is a test of the emergency broadcast system"}
               additions))

        (is (= {:b "so is this"}
               deletions))))))
