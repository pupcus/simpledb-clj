(ns helpers
  (:require [clojure.core.async :as a]
            [clojure.string :as str]
            [taoensso.timbre :as log]
            [cognitect.aws.credentials :as creds]
            [cognitect.aws.http :as http]
            [cognitect.aws.region :as region]
            [cognitect.aws.signers.v2]
            [helpers :as h]
            [simpledb.util :as u]))

(log/set-level! :info)

;;
;; convert well formed maps to xml
;;

(defn clj->xml
  "minimal/incomplete impl that transforms clojure vecs to xml strings"
  [v] {:pre [(vector? v)]}

  (letfn [(val->xml [target]
            (cond
              (map? target)
              (str/join (mapv vec->xml target))

              (vector? target)
              (vec->xml target)

              :otherwise
              (str target)))

          (vec->xml [[root val & rest]]
            (let [s  (format "<%s>%s</%s>" (name root) (val->xml val) (name root))]
              (if-not (empty? rest)
                (str s (vec->xml rest))
                s)))]

    (vec->xml v)))

;;
;; build an xml response string from a vector map
;;

(defn build-sdb-response  [{:keys [op response]}]
  (let [root (name op)]
    (str/join [(format "<%sResponse>" root)
               (when response (clj->xml response))
               (clj->xml [:ResponseMetaData {:RequestId "REQUEST_ID"
                                             :BoxUsage "BOX_USAGE"}])
               (format "</%sResponse>" root)])))


;;
;; convert strings <---> bytebuffers
;;

(defn str->bb [s]
  (java.nio.ByteBuffer/wrap (.getBytes s)))

(defn bb->str [bb]
  (let [bytes  (byte-array (.remaining bb))]
    (.get bb bytes)
    (String. bytes)))


;;
;; stubs for making mock clients (pilfered from cognitect aws test code :-) no shame)
;;

(defn stub-http-client [result]
  (reify http/HttpClient
    (-submit [_ _ ch]
      (a/go (a/>! ch result))
      ch)
    (-stop [_])))

(defn stub-credentials-provider
  ([] (stub-credentials-provider {}))
  ([creds]
   (let [key (or
              (get creds :access-key-id)
              (get creds :aws/access-key-id)
              "MOCKACCESSKEYEXAMPLE")
         secret (or
                 (get creds :secret-access-key)
                 (get creds :aws/secret-access-key)
                 "MOCKSECRETKEYEXAMPLE7y/PmFz9iisjU8i/GT4E")
         creds {:aws/access-key-id key
                :aws/secret-access-key secret}]

     (reify creds/CredentialsProvider
       (fetch [_] creds)))))

(defn stub-region-provider
  ([] (stub-region-provider :us-east-1))
  ([region]
   (reify region/RegionProvider
     (fetch [_] region))))

;;
;; overrides for credentials and region providers in mock clients

(def client-overrides
  {:credentials-provider (stub-credentials-provider)
   :region-provider (stub-region-provider)})

;;
;; capture requests to amazon from cognitect aws http clients
;;

(def timestamp "2020-06-16T09:12:40.316Z")
(defmacro with-captured-requests [capture-var action-response-map & body]
  `(let [~capture-var (atom {})]
     (letfn [(mock-http# [client# op-map# channel#]
               (let [body# (bb->str (:body op-map#))
                     [_# action#] (re-find #"Action=([^&]*)" body#)
                     current# (get (deref ~capture-var) (keyword action#) [])]
                 (swap! ~capture-var assoc (keyword action#) (conj current# {:op-map (assoc op-map# :body-as-str body#)}))
                 (a/>!! channel# {:status 200 :body (str->bb (get ~action-response-map (keyword action#)))})))]
       (with-redefs [cognitect.aws.signers.v2/timestamp (fn [] ~timestamp)
                     http/submit mock-http#]
         ~@body))))

;;
;; once the request is made the body of the request is a param string
;; this helper splits the string into param=value snippets and
;; splits them again into k,v pairs and puts them into a sorted map so we can test what is there
;;

(defn body-params-as-map [body]
  (reduce
   (fn [m param]
     (let [[k v] (str/split param #"=" 2)
           curr (get m k)]
       (if curr
         (update m k conj (u/ensure-vec curr) v)
         (assoc m k v))))
   (sorted-map)
   (str/split body #"[&]")))
